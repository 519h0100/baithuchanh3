import 'dart:async';

class TextStream {

  Stream<String> getText() async* {
    final List<String> texts = [ "Hi guys, how are you going", "i'm almost finshed this level", "Hi Nate", "Hey, you are late", "How old are you Nate"];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % 5;
      return texts[index];
    });
  }
}
